# Glance SDKs
## Release Notes

### Changes in 5.7.0

**All Platforms**
- `Visitor.init` and `VisitorPresence.connect` are now asynchronous.  This can prevent iOS Watchdog app termination on devices without Internet connectivity.  NOTE: implementations MUST wait for `EventVisitorInitialized` before starting sessions, and `EventPresenceConnected` before calling other presence methods.
- A NULL check has been added for string parameters passed to SDK methods.  A NULL value formerly could cause a crash on some platforms, now an empty string will be used and a debug message logged.

**Android**
- A bug was fixed where `name`, `email`, `phone` parameters passed to `Visitor.init` were ignored.
- The `VisitorInitParams` structure has been extended with all parameters available for Visitor.init
- Fixed a bug where default UI would appear when turned off in certain situations.
- NOTE: There is a known issue limited to a minority of Android devices where when the device is rotated, some masked fields inside of web views may be partially visible.

### Changes in 5.4.7

**Android**
- Fix potential rendering bugs and crash with JetPack Compose or ExperimentalCoilApi
- Fix crash in Presence ShowTerms
- Default UI string for `GLANCE_DEFAULT_UI_START_DIALOG_HEADSET_TEXT` updated to match iOS

### Changes in 5.3.0

**iOS**
- Minimum iOS version is now 11.0
- Ability to show Scene or SwiftUI based apps

**Android**
- Fixes for masking of keyboard
- Fixes and performance improvement to agent video on older Android OS versions

**iOS and Android**
- Agent role is now sent in `EventGuestCountChanged` on `guestlist` property. Name, title, username etc. may optionally be sent
- Capability for Dynamic Masking based on agent role or other information
- `StartParams.paused` option to start sessions in a paused (hidden) state
- Added events `glancePresenceDialogDidTapYes`, `glancePresenceDialogDidTapNo` tapping on decline or accept within Presence terms dialog 
- Flag is stored on sessions started with Presence
- Localized strings are now consistent on iOS and Android

### Changes in 5.2.0

**iOS**
- The SDK is now built with XCode 13.0 and supports iOS 15
- Allow overriding of images in the default UI warning dialog
- Fix freeze when web view masking used with inaccessible iframe, and add console error

**Android**
- The SDK build has been upgraded to Gradle 7.0 and Build Tools 30.0 and supports Android 12
- Bug fix in initiating screenshare via Presence
- Fix freeze when web view masking used with inaccessible iframe, and add console error
- Fix crash and add error check for using web view masking before Visitor.Init() called

### Changes in 5.1.1

**Android**
- Fix potential crash if removeMask is called multiple times

### Changes in 5.1.0

**iOS**
- Masking of HTML elements within a WKWebView
-  Fix a rare potential crash on iOS on force-quitting an application right after initializing the Glance SDK with poor network connectivity
- arm64 simulator is now supported

**Android**
- Masking of HTML elements within a WebView

### Changes in 4.19.0

**iOS**
- The SDK is now available as an XCFramework and is built with XCode 12.4
- NOTE: .framework files are still supplied, when using these with Xcode 12+ set your Project Settings, Build System to "Legacy Build System"
- When voice is used the TwilioVoice SDK must be updated to 6.2.2 XCFramework
- Glance_iOS and Glance_iOSVideo frameworks drop support for 32-bit (arm7) devices.  There is a Glance_iOSLegacy framework available with 32-bit support.  It does not support Twilio Voice.
- Add capability to localize more strings in the Default UI
- `GlanceDefaultUIDelegate` methods to support secure server-side authentication for Voice with the Default UI
- Add a message shown to the agent when the app is suspended
- Support for labels shown to agent on masked elements. New method: `addMaskedView:withLabel:`
- Various bugs fixes

**Android**
- Add capability to localize more strings in the Default UI
- `DefaultUIVoiceAuthenticationListener` interface to support secure server-side authentication for Voice with the Default UI
- Add a message shown to the agent when the app is suspended
- Support for labels shown to agent on masked elements. New method: `addMaskedView(View view, String label)`
- New method `DisplayParams.setDisplayId` to select display to show on multi-display devices
- Various bug fixes


### Changes in 4.14.3

**iOS**
- Fix potential crash on application termination while a Glance session is running

### Changes in 4.14.2

**iOS**
- Allow customization of default UI video message "You'll see live video of your agent but the agent won't see you" (key is GLANCE_DEFAULT_UI_START_DIALOG_VIDEO_TEXT)

**Android**
- Fixed an issue that masked the entire screen in the agent view when the visitor would scroll past a masked object.

### Changes in 4.14.1

**Android**
- Added option StartParams.setMediaProjectionEnabled to use the MediaProjection API for screen capture. This option may be necessary to capture certain types of content, for example from the Google Maps SDK.

### Changes in 4.14.0

**iOS and Android**
- See details on new app originated and 2-way video below in 4.13.0
- Various small bug fixes and stability improvements

**iOS**
- When voice is used the Twilio SDK must be updated to 5.3.1

**Android**
- When voice is used the Twilio SDK must be updated to 5.4.0
- Option to use MediaProjection to show all apps updated for Android 11 and added ability to use with Presence (One-click) initiated sessions

### Changes in 4.13.0 (beta)

**iOS and Android**
- Ability to start video sessions from app to agent with front or back camera
- 2-way (app and agent) video, including a default UI
- Native coded video viewer for improved agent video performance
- Various bug fixes and security, stability and performance improvements

**iOS**
- On iOS there are two versions of the SDK: 
    - Glance_iOSVideo.framework is used when 2-way video is required
    - Glance_iOS.framework is smaller and does not support 2-way video.  It still supports agent video.
- SocketRocket framework is no longer embedded
- Mitigation for potential crash on app exit when UIApplicationWillTerminateNotification is not sent

### Changes in 4.8.5

**iOS**
- Fix potential crash on initialization of framework

**Android**
- Add orange border to default UI during session (parity with iOS)
- Fix EventChildSessionEnded not posted when video ended
- Fix UI remains visible after ending video then main session

### Changes in 4.8.2

**iOS**
- Fix potential crash when force-quitting application

**Android**
- Fix occasional freeze of agent video (video from agent on browser works better with Chrome 78 installed on the Android device)
- Fix intermittent crash with large number of event listeners
- Improve display of terms and conditions on default user interface 
- Fix TLS issue which could cause an SSLContext initialization error.  Android SDK now supports TLS 1.3 on devices using Android 10+


### Changes in 4.8.1

**iOS**
- Update to Twilio SDK 2.1
- Minor fixes to default UI

**Android**
- Update to Twilio SDK 2.1
- Automatically scale capture better for high or low density displays
- Add StartParams `scale` parameter to manually adjust capture scaling

### Changes in 4.7.2

**iOS and Android**
- 15020 Allow calling Visitor.init again with visitor id
- Small fixes to Visitor and Presence

**Mac**
- 14943 Build with XCode 10.2.1 for Swift ABI compatibility

### Changes in 4.7.1

**iOS and Android**
- A default UI for screenshare, video and voice is now available*
- GlanceVoice class moved from separate SDK to main SDK*

**iOS**
- initVisitor method supplied, callable from Swift 5*

**All platforms**
- Agent video can be shown by an agent connecting with Chrome (Windows SDK requires your app to include an embedded browser)
- 14874 Underscore is now allowed in a Presence visitor id
- 14430 For manually started session, different key from Presence visitor id can be specified


### Changes in 4.5.1

**All platforms**
- 14180 Presence/One-Click session start by agent on Salesforce
- small fixes to Presence timing and edge cases of start session/show terms prompt

**iOS**
- 14075 Glance Glance gesture window should not become key window
- 14143 add setGlanceWindowTag: method to set window tag of gesture window*
- 14258 add setContentMode method to set UIViewContentMode of agent video viewer*

### Changes in 4.5.0

**All platforms (except Mac):**
- Implementation of PresenceVisitor for Signaling and agent One-Click session connect (NOTE: this release does not yet support Glance for Salesforce and optional argument passing to invokeVisitor in the Presence Agent API)

**Android**
- 14099 Support for custom vector drawable Agent cursor 

### Changes in 4.4.4

**Android**
- 13781 Replace libpng, libcurl, libssl, libcrypto with native code, reducing SDK size
- Fix support for TLS 1.2 on older OS versions
- Bug fixes, especially improve stability of agent video

**Windows**
- 13903 Agent Video window remains on top
- 13721 Fixes to capture with monitor scaling past 100%

**Mac**
- 13903 Agent Video window remains on top
- 13630 Fix scroll wheel doesn't scroll on Mac when viewing from browser viewer
- 13414 Replace deprecated class/method/function usages


### Changes in 4.4.3

**iOS**
- 13679 Fix crash adding delegate in test environment without autorelease pool

**All platforms:**
- 13655 Fix EventVisitorInitialized posted before Init is finished
- 13152 Don't start event thread until SDK is used

### Changes in 4.3.1

**iOS**
- 13210 Update for iOS 12 change that affected performance
- 12841 Fix to keyboard masking in Visitor Sessions
- 12414 Add Visitor.pause method *
- Bug fixes

**Android**
- 12842 Masking by view ID *
- 12843 Visitor session start with StartParams and full screen support *
- 13400 Support for custom agent video viewer *
- Bug fixes

### Changes in 4.1.1 (iOS only)

**iOS**
- 12414 Add pause method to Visitor API *
- 12435 Pause/Resume screensharing when app resigns/becomes active app
- 12440 Mask fields even when hidden, due to edge/race condition
- 12433 Fix Memory leak in HttpRequest of NSMutableURLRequest
- 12429 Ensure idleTimerDisabled is run on main thread
- 12438 Fix crash when suspending app on iPad Mini (iOS 9.3.5)


### Changes in 4.1.0

**iOS**
- 12104 Replace deprecated CFNetwork library for HTTPS
- 12151 Keyboard masking method *
- 12172 Fix: Gesture pointer on customer side is slightly to the right of where the agent is pointing.
- 12326 Remove "assign" property for customViewerDelegate *

**Android**
- 12148 Document per-architecture builds using build.gradle "splits" declaration
- 12149 Update deprecated Gradle dependencies
- 12150 Eliminate Glide source files
- 12195 Make EventType public and accessible *
- 12307 Remove use of Kotlin (rewrite in Java)  
- Fix several small bugs in Agent Video  

**All platforms:**
- 12140 Upgrade libpng to 1.6.34 [zlib and jpeglib already at current version]
- Add several checks against incorrect API usage

 &#42; API Changes
 
### Changes in 4.0
 
 **All platforms**
 - Simple Visitor API
 
