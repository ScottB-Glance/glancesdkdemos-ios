# One-click Connect
## _Visitor Presence and Signaling API_

## Introduction
Glance Presence and Signalling allows an identified iOS application user to notify
agent CRM or support systems that the user is actively using the app.  The agent side
may then signal the app to start screensharing.  The agent can initiate and join the
screenshare automatically or with one-click.

This document is a Quick-start guide for integrating One-click Connect Screensharing
into an iOS application.  It assumes you have already integrated the Glance iOS SDK
(Framework) as described in the [README](README.md) file.

This functionality is available starting in version 4.5.0 of the SDK.
For full documentation on the Presence Visitor API see [Mobile SDK One-Click Connect]( https://help.glance.net/mobile-app-sharing/user_guide/presence_sdk/).

## Integration
Integration uses the `GlanceVisitor` and `GlancePresenceVisitor` classes.  All methods
must be called on the main (UI) thread.

### Initialization
In your call to `GlanceVisitor init:` add the additional parameter `visitorid`.
The value is a string that uniquely identifies this user.  This is your identifier
for the user.  The same visitor id would be used when the same visitor is logged
into the app on another device, or in a web application instrumented for Glance
One-click Cobrowse.

```swift
    // Configure Glance Visitor SDK
    GlanceVisitor.init(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "", visitorid: VISITOR_ID)
```
The visitor id will also be used as the session key for any screensharing session.

### Events
Presence events are delivered to your `GlanceVisitor glanceVisitorEvent()` delegate
method.  Note that events are delivered on the SDK event handling thread, NOT the
main (UI) thread.

You may want to handle these:

```swift
        case EventPresenceConnected:
            print("Presence connected")
            break

        case EventPresenceConnectFail:
            print("Presence connect failed: ", event.message)
            break

        case EventPresenceSendFail:
            print("Presence send failed: ", event.message)
            break
```

### Starting and Stopping
To start Presence after receiving EventVisitorInitialized call `connect`

```swift
    GlancePresenceVisitor.connect()
```

To stop (normally on application exit) call `disconnect`

```swift
    GlancePresenceVisitor.disconnect()
```

### Navigation Information
When Presence is connected, information can be sent to the agent side
whenever the user navigates through the app.

```swift
    GlancePresenceVisitor.presence(["url": "Product page 1"])
```

When your app becomes active or enters the background state the SDK will also
notify the agent side.

### Local Notifications

Presence has the ability to alert the user when the app enters the background state. If the agent reaches out to your app while it is backgrounded, a local notification can be triggered by the SDK. This will require the notification permission from your app. You can implement this yourself by asking the user, or you can use the following:

```swift
    GlancePresenceVisitor.connect(true);
```

which, on connecting to Presence, will prompt the user to allow push notifications (if they haven't already accepted). Once accepted, notifications will be sent to the user when the app is in the background and an agent attempts to make contact with the user.

### Customizing the UI
The SDK provides a default user interface to request confirmation and
show Terms and Conditions.  The URL to a Terms web page is configured in the
agent-side CRM system.

To provide your own UI, after calling `connect` call:

```swift
    GlancePresenceVisitor.setDefaultUI(false)
```

Then handle the `EventPresenceShowTerms` event.
In your event handing you should:
1. Display your terms and conditions and options to accept (start screenshare) or decline.
2. Notify the agent terms have been displayed
3. Notify the agent of accept or decline
4. On accepting, call `GlanceVisitor.startSession`

An example:

```swift
        case EventPresenceShowTerms:
            myCustomUI(termsUrl: event.properties["termsurl"] as! String)
            break
```

```swift
    func myCustomUI(termsUrl: String) {
        DispatchQueue.main.async {
            let actionSheet = UIAlertController(title: "Allow the agent to view this app?", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)

            actionSheet.addAction(
                UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) in
                    GlancePresenceVisitor.signalAgent("terms", map: ["status": "accepted"])
                    GlanceVisitor.startSession()
            }))

            actionSheet.addAction(
                UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { (action) in
                    GlancePresenceVisitor.signalAgent("terms", map: ["status": "declined"])
                }))

            actionSheet.addAction(
                UIAlertAction(title: "Show Terms", style: UIAlertActionStyle.default, handler: { (action) in
                    // show terms url in web view here. you can use the termsURL or not
                    GlancePresenceVisitor.signalAgent("terms", map: ["status": "displayed"])
                }))

            self.present(actionSheet, animated: true, completion: nil)
        }
    }

```


