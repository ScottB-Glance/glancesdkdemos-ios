# Agent Info and Dynamic Masking in the iOS SDK

## Basic SDK Usage

For basic SDK Usage and the Visitor API see the other sample projects such as [ScreenshareDemo_swift](https://gitlab.com/glance/glancesdkdemos-ios/-/tree/master/ScreenshareDemo_swift)

## Agent Information

Agent (and other guest) information will be passed to your application on the `EventGuestCountChanged` event.  You can handle this event in your event handler function.

The list of guest information is a JSON array on the `"guestlist"` property of the event.  For example:

```[{"role":"visitor"},{"agentrole":"Supervisor","name":"Dr. Rich Baker","partnerid":12345,"partneruserid":"rbaker","role":"agent","title":"Gentleman + Scholar","username":"rich.glance.net"}]```

Note that `"role"` is the participant role (one of `"visitor"`, `"agent"`, `"guest"`, `"host"`), while `"agentrole"` is the name of the Role (if any) set up for Role-based permissions.

By default accounts are configured to send only the `"role"` and the `"agentrole"` for agents, but sending of other agent information such as name, username and partneruserid can be enabled.

The agent information passed on guestlist can be useful for implementing masking based on agent role, username, or partneruserid. 

## Handling the event

You will provide an event handler for the `EventGuestCountChanged` like other events in your delegate method, something like this:

```swift
    // Glance Visitor delegate
    func glanceVisitorEvent(_ event: GlanceEvent!) {
        switch event.code {

        case EventGuestCountChange:
            // read JSON guest list
            if let sguestlist = event.properties["guestlist"] as? String {
                print("guestlist: ", sguestlist)
                let json = try? JSONSerialization.jsonObject(with: sguestlist.data(using: .utf8)!, options: [])
                if let guests = json as? [Any] {
                    DispatchQueue.main.async {
                        self._handleGuestList(guests);
                    }
                }
            }
            break;

        case EventSessionEnded:
```

## Role-based Masking

The agent information passed on `guestlist` can be useful for implementing masking based on agent role, username or partneruserid.

You will need to handle any masking, pausing of screenshare, or any customization of your application you wish to do based on agents or agent roles.

This code from the sample will pause screensharing whenever any agent joins with a role named `"pause"`.  Otherwise it will mask two fields if any agent with role `"mask"` joins.  If only agents in other roles join, there will be neither pausing nor masking:

```swift
    func _handleGuestList(_ guests: [Any]) {
        var masked: Bool = false;
        var paused: Bool = false;
        for object in guests {
            if let guest = object as? [String: Any] {
                if let role = guest["role"] as? String {
                    if (role == "agent") {
                        if let agentrole = guest["agentrole"] as? String {
                            // here is where we check for role-based masking/pausing
                            if (agentrole == "pause") { paused = true }
                            if (agentrole == "mask") { masked = true }
                        } else {
                            print("agentrole missing from guestlist agent")
                        }
                    }
                } else {
                    print("guest role missing")
                }
            }
        }
        
        // here is where the actual role-based masking/pausing happens
        GlanceVisitor.pause(paused);
        if (masked) {
            GlanceVisitor.addMaskedView(groupId)
            GlanceVisitor.addMaskedView(sessionKey)
        } else {
            GlanceVisitor.removeMaskedView(groupId)
            GlanceVisitor.removeMaskedView(sessionKey)
        }
    }
```

### Starting Paused

When a screenshare session starts, the contents of the screen are being streamed to the Glance service even before any agent joins.  If an agent with a limited role joins they may briefly see inappropriate information before the masking or pausing above is applied.  

In order to prevent that the session should be started in a paused state.  Then when an agent joins the correct masking can be applied before the session is "unpaused".

To start a session in a paused state you call the `GlanceVisitor.startSession` method with `StartParams` like this:

```swift
        let startParams: GlanceStartParams = GlanceStartParams()
        startParams.key = sessionKey // or "GLANCE_KEYTYPE_RANDOM"
        startParams.paused = true    // this will cause the session to start paused
        GlanceVisitor.startSession(with: startParams)
```

### Multiple Roles

If an agent has joined and screensharing is enabled (not paused) and an additional agent were to join with a more limited role, this agent could briefly see inappropriate information.  To prevent this situation your Glance account can be configured to disallow additional agents with different roles from joining a session.


## Support

Email [mobilesupport@glance.net](mailto:mobilesupport@glance.net) with any questions.

