//
//  AgentCell.swift
//  RoleMaskingDemo_Swift
//
//  Created by Ed Hardebeck on 1/4/22.
//  Copyright © 2022 Glance Networks. All rights reserved.
//

import UIKit

class AgentCell: UITableViewCell {
    @IBOutlet weak var agentName: UILabel!
    @IBOutlet weak var agentRole: UILabel!
}
