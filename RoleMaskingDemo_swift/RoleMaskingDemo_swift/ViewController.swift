//
//  ViewController.swift
//  RoleMaskingDemo_swift
//
//  Copyright © 2022 Glance Networks. All rights reserved.
//

import UIKit

import Glance_iOS

// Visitor demo implementation is below. NOTE:
// before reading through this code, make sure you've read through the README

let GLANCE_GROUP_ID : Int32 = 15687  // Get this from Glance

// make sure your interface implements GlanceVisitorDelegate
// Uncomment GlanceCustomViewerDelegate to test the custom agent viewer
class ViewController: UIViewController & UITableViewDelegate, UITableViewDataSource, GlanceVisitorDelegate, GlanceDefaultUIDelegate {
    @IBOutlet weak var openBrowserButton: UIButton!
    @IBOutlet weak var startSessionButton: UIButton!
    @IBOutlet weak var endSessionButton: UIButton!
    @IBOutlet weak var sessionKeyLabel: UILabel!
    @IBOutlet weak var presenceIndicator: UILabel!
    @IBOutlet weak var glanceServer: UITextField!
    @IBOutlet weak var groupId: UITextField!
    @IBOutlet weak var sessionKey: UITextField!
    @IBOutlet weak var agentList: UITableView!
    
    var customWindow : UIWindow?
    
    // Uncomment the VISITOR_ID value to enable Presence code
    // If you are not using Presence you do not need to implement any code within if (VISITOR_ID != nil)
    var VISITOR_ID : String? // = "123four" // This is your identifier for the logged in (or identified) user.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Make sure the GlanceVisitor is listening for events on this ViewController
        GlanceVisitor.add(self)
         
        // This is not necessary in production code, the server defaults to www.glance.net
        // GlanceSettings().set(kGlanceSettingGlanceServer, value: "www.myglance.net")

        if (VISITOR_ID != nil) {
            // Init with visitor id for Presence, you would probably do this on user login
            GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "", visitorid: VISITOR_ID)
        } // else {
        // Init for screensharing, Normally the group id is fixed and you would do this once on application launch
        // GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
        // }
        
        GlanceVisitor.defaultUI(true, delegate: self, termsUrl: nil)

        // This view controller itself will provide the delegate methods and row data for the table view.
        agentList.delegate = self
        agentList.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (VISITOR_ID != nil) {
            GlancePresenceVisitor.presence(["url": "main view"])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startSession(_ sender: UIButton) {
        
        GlanceSettings().set(kGlanceSettingGlanceServer, value: glanceServer.text ?? "www.glance.net")
        
        if (VISITOR_ID == nil) {
            let glanceGroupId: Int? = Int(groupId.text!)
            GlanceVisitor.initVisitor(Int32(glanceGroupId!), token: "", name: "", email: "", phone: "")
        }
        
        let startParams: GlanceStartParams = GlanceStartParams()
        startParams.key = sessionKey.text!.isEmpty ? "GLANCE_KEYTYPE_RANDOM" : sessionKey.text!
        startParams.paused = true
        GlanceVisitor.startSession(with: startParams)
    }
    
    @IBAction func endSession(_ sender: UIButton) {
        // End Glance session
        GlanceVisitor.endSession()
    }
    
    func _sessionStarted(_ sessionKey: String) {
        DispatchQueue.main.async {
            self.sessionKeyLabel.text = sessionKey
            // Adds a masked view to sessionLabel which will keep it from being seen by the agent
            GlanceVisitor.addMaskedView(self.sessionKeyLabel)
            self.startSessionButton.isHidden = true
            self.endSessionButton.isHidden = false
        }
    }
    
    func _sessionEnded() {
        DispatchQueue.main.async {
            self.sessionKeyLabel.text = ""
            // Remove masked view
            GlanceVisitor.removeMaskedView(self.sessionKeyLabel)
            self.startSessionButton.isHidden = false
            self.endSessionButton.isHidden = true
            self.agents = []
            self.agentList.reloadData()
        }
    }
    
    func alertWithTitle(_ title: String, _ message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    var agents: [(name:String, role:String)] = []
    
    func _handleGuestList(_ guests: [Any]) {
        var masked: Bool = false;
        var paused: Bool = false;
        agents.removeAll()
        for object in guests {
            if let guest = object as? [String: Any] {
                if let role = guest["role"] as? String {
                    if (role == "agent") {
                        if let agentrole = guest["agentrole"] as? String {
                            let name: String = guest["name"] as? String ?? "<no name>"
                            print(name, " - ", agentrole)
                            agents.append((name: name, role: agentrole))
                            // here is where we check for role-based masking/pausing
                            if (agentrole == "pause") { paused = true }
                            if (agentrole == "mask") { masked = true }
                        } else {
                            print("agentrole missing from guestlist agent")
                        }
                    }
                } else {
                    print("guest role missing")
                }
            }
        }
        
        // here is where the actual role-based masking/pausing happens
        GlanceVisitor.pause(paused);
        if (masked) {
            GlanceVisitor.addMaskedView(groupId)
            GlanceVisitor.addMaskedView(sessionKey)
        } else {
            GlanceVisitor.removeMaskedView(groupId)
            GlanceVisitor.removeMaskedView(sessionKey)
        }
        
        agentList.reloadData()
    }
    
    // Glance Visitor delegate
    func glanceVisitorEvent(_ event: GlanceEvent!) {
        switch event.code {
        case EventSessionEnded:
            // WAIT for this event to fire before starting a new session
            // Show any UI to indicate the session has ended
            _sessionEnded()
            break
        case EventConnectedToSession:
            // Show any UI to indicate the session has started
            // event.properties includes the sessionKey which can be
            // displayed to the user to read to the agent
            _sessionStarted(event.properties["sessionkey"] as! String)
            break
        case EventGuestCountChange:
            // read JSON guest list
            if let sguestlist = event.properties["guestlist"] as? String {
                print("guestlist: ", sguestlist)
                let json = try? JSONSerialization.jsonObject(with: sguestlist.data(using: .utf8)!, options: [])
                if let guests = json as? [Any] {
                    DispatchQueue.main.async {
                        self._handleGuestList(guests);
                    }
                }
            }
            break;
            
        case EventStartSessionFailed:
            _sessionEnded()
            break
        default:
            // Best practice is to log all other events of type EventError, EventWarning, or EventAssertFail
            switch(event.type) {
            case EventError:
                alertWithTitle("Error", event.message)
                break
            case EventAssertFail:
                alertWithTitle("Assert Fail", event.message)
                break
            case EventWarning:
                break
            default:
                break
            }
        }
        if (VISITOR_ID != nil) {
            switch event.code {
            case EventVisitorInitialized:
                DispatchQueue.main.async {
                    GlancePresenceVisitor.connect()
                }
                break;
            
            case EventPresenceConnected:
                DispatchQueue.main.async {
                    self.presenceIndicator.isHidden = false
                    self.startSessionButton.isHidden = true
                }
                break;
            
            case EventPresenceConnectFail:
                print("Presence connection failed (will retry): ", event.message)
                break;
            
            case EventPresenceShowTerms:
                // You only need to handle this event if you are providing a custom UI for terms and/or confirmation
                print("Agent signalled ShowTerms")
                break;
            
            case EventPresenceBlur:
                // This event notifies the app that the visitor is now using another app or website
                break;
            default:
                break;
            }
        }
    }
    
    // Agent list UITableView
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return agents.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AgentCell = self.agentList.dequeueReusableCell(withIdentifier: "cell") as! AgentCell
        
        cell.agentName.text = agents[indexPath.row].name
        cell.agentRole.text = agents[indexPath.row].role
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
}

