# Visitor SDK for iOS

## Setup


Before you get started you'll need a Glance account.  

Contact Glance at mobilesupport@glance.net to set up a Group (organization account) and an individual Agent account.
You will receive:
1. A numeric Group ID
2. An agent username, for example agnes.example.glance.net (Glance usernames are in the form of DNS names)
3. An agent password

In production usage, agents will typically authenticate with a single-sign-on, often via their CRM implementation (e.g. Salesforce).


## Integration

## Quick start

Clone this project and replace the GLANCE_GROUP_ID value with your Group ID obtained from Glance.
Build for debugging. A Release build will require you to set the project Code Signing

## Installation

### Framework

Download the Glance iOS SDK (link needed) or copy from this project
Add and copy the Glance_iOS.framework to your Xcode project.
Ensure that Glance_iOS.framework is listed within Embedded Binaries and within Linked Libraries and Frameworks.

### Configuration

Now that you have the Glance framework imported into your Xcode project and have obtained your Glance Group ID you can get start integrating the Visitor SDK.

In your AppDelegate add an import for the Glance iOS Framework headers:

```swift
import Glance_iOS
```

Within the AppDelegate didFinishLaunchingWithOptions method add the following configuration with your GLANCE_GROUP_ID number which is required.  The token, name, email and phone fields are optional.

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
    return true
}
```

#### Default UI

Glance provides a default user interface for screenshare, agent video and voice sessions.  This interface is enabled by default.  To build your own custom user interface the default UI needs to be disabled:

```swift
GlanceVisitor.defaultUI(false);
```

#### ReplayKit

On devices with iOS 11.0 or greater [ReplayKit](https://developer.apple.com/documentation/replaykit?language=objc) can be utilized.  ReplayKit is an Apple provided, performance optimized API designed specifically for recording the screen.

GlanceVisitor has ReplayKit disabled by default.  To enable ReplayKit for GlanceVisitor:

```swift
GlanceVisitor.setReplayKitEnabled(true);
```

When starting a GlanceVisitor session with ReplayKit enabled a permissions dialog will show asking the user for permission to record the screen.  The title of the dialog will display **"Allow screen recording in APPNAME?"** and the options are **Record Screen** and **Don't Allow**.  The screenshare will proceed with ReplayKit if the user selects **Record Screen**.

One important note about ReplayKit is that it will prevent apps on iPhone devices from rotating while being used.  The interface orientation at the time the screenshare session starts will be maintained until the session is over.  iPad devices can rotate freely with ReplayKit.

If your app heavily depends on the ability to rotate on the iPhone, then ReplayKit may not be something you want to use.  Otherwise, ReplayKit is a better screen recording experience from both a quality and performance perspective for both the end user and the screenshare agent.

ReplayKit only works on an iOS device and will not work within the simulator.  When integrating and testing be sure to do so on device.

### Events

Before starting a session you should register a GlanceVisitorDelegate delegate to monitor session start and end events.

```swift
class ViewController: UIViewController & GlanceVisitorDelegate
```

The GlanceVisitorDelegate is a protocol with the following definition:

```objc
@protocol GlanceVisitorDelegate
    -(void)glanceVisitorEvent:(GlanceEvent*)event;
@end
```

The glanceVisitorEvent method can receive a variety of GlanceEvent code types.  The important ones for the visitor implementation are: EventConnectedToSession, EventStartSessionFailed and EventSessionEnded.  Here is an example implementation:

```swift
func glanceVisitorEvent(_ event: GlanceEvent!) {
    switch event.code {
    case EventConnectedToSession:
        // Successfully connected to session
        // Get generated session key
        String sessionKey = event.properties["sessionkey"]
        break
    case EventStartSessionFailed:
        // Couldn't start session
        // Error message can be found in event.message
        break
    case EventSessionEnded:
        // Session ended
        break
    default:
        // It is recommended that you at least log all other events to help debug during development
        // Best practice is to log all other events of type EventAssertFail, EventError, or EventWarning
        break;
    }
}
```

The delegate can be registered to receive events with the following call:

```swift
GlanceVisitor.add(self)
```

### Starting a Session

There are two ways to start a session: with and without a session key.  If no session key is provided then one will be generated and returned in the properties of the GlanceEvent with code EventConnectedToSession.

To start a session without a session key:

```swift
GlanceVisitor.startSession()
```

To start a session with a specified session key:

```swift
GlanceVisitor.startSession("1234")
```

### Stopping a Session

To stop a visitor session in progress make the following call:

```swift
GlanceVisitor.endSession()
```

## Testing

### Confirm it's working

To confirm the integration is working, start the session and note the session key.

The agent should then go to:
<https://www.glance.net/agentjoin/AgentJoin.aspx>.  

If not logged in already, the agent will be asked to login:

![](images/agent1.png)

When logged-in a form will be shown to enter the session key.

![](images/agent2.png)

At that point the web viewer will display the screen being shared with the ability to gesture:

![](images/agent3.png)

When the key is known a view can be opened directly with
<https://www.glance.net/agentjoin/AgentView.aspx?username={username}&sesionkey={key}&wait=1>

See (other documentation) for details on integrating the agent viewer with other systems and single-sign-on

## Advanced

### Masking

Views with personal private information or banking information should be hidden from the agent.  You can mask a view like this:

```swift
GlanceVisitor.addMaskedView(self.creditCardNumberTextField)
```

You can remove a mask with another call:

```swift
GlanceVisitor.removeMaskedView(self.creditCardNumberTextField)
```

You must remove any masked views before the view itself is deallocated.


### Custom Viewer

You can customize the appearance and behavior of the agent video viewer by implementing the `GlanceCustomViewerDelegate`.

```objc
/**
 * A delegate to manage the agent viewer video experience.
 */
@protocol GlanceCustomViewerDelegate

/**
 * Called when the agent viewer starts with a supplied UIView and size.
 *
 * @param glanceView    UIView displaying agent video.  Add this view to your interface.
 * @param size          Preferred size of the UIView
 */
-(void) glanceViewerDidStart:(UIView*)glanceView size:(CGSize)size;

/**
 * Called when the agent viewer has stopped
 *
 * @param glanceView    UIView displaying agent video.  Remove this view from your interface.
 */
-(void) glanceViewerDidStop:(UIView*)glanceView;
@end
```

In order to enable the integration you need to set the custom viewer delegate by calling the following method:

```swift
GlanceVisitor.setCustomViewerDelegate(self)
```

The demo application has a `GlanceCustomViewerDelegate` implementation that enables the agent video viewer to be dragged around the screen.  This is done by implementing a `CustomViewerWindow` class that takes the UIView provided to `glanceViewerDidStart:`, places it in a window that floats above everything else and captures touch events to handle dragging.

### Building for Release

The Glance_iOS.framework includes support for all types of architectures to support development deploying to both the simulator and device.

When building for release only device architectures should be exported.  In order to remove unnecessary architectures a build phase has to be added.  The script will apply to the Glance library and any libraries within the app.  Only one strip frameworks script is required per project.

A copy of [strip-frameworks.sh](https://gitlab.com/glance/GlanceSDKDemo_iOS_Swift/blob/master/strip-frameworks.sh) should be placed in root of the project directory.

![](images/buildPhase1.png)

Next, add a build phase to your Xcode project.  Open the project configuration and navigate to Build Phases.

![](images/buildPhase2.png)

Then click on New Run Script Phase.

![](images/buildPhase3.png)

Paste the following into the code area:

```bash
bash "${SRCROOT}/strip-frameworks.sh"
```

![](images/buildPhase4.png)

That's it!

## Support

Email [mobilesupport@glance.net](mailto:mobilesupport@glance.net) with any questions.

