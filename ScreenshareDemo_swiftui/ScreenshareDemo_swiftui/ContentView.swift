//
//  ContentView.swift
//  ScreenshareDemo_swiftui
//
//  Created by Kyle Shank on 2/3/22.
//

import SwiftUI
import Glance_iOS

class MyGlanceVisitorDelegate : ObservableObject, GlanceVisitorDelegate {
    @Published var inSession = false
    
    func glanceVisitorEvent(_ event: GlanceEvent!) {
        DispatchQueue.main.async {
            switch event.code {
            case EventSessionEnded:
                self.inSession = false
                break
            case EventConnectedToSession:
                self.inSession = true
                break
            case EventStartSessionFailed:
                self.inSession = false
                break
            default:
                break
            }
        }
    }
}

struct ContentView: View {
    @State var inSession : Bool = false
    @ObservedObject var glanceVisitorDelegate = MyGlanceVisitorDelegate()
    
    var body: some View {
        Button(self.$glanceVisitorDelegate.inSession.wrappedValue ? "Stop Session" : "Start Session") {
            self.$glanceVisitorDelegate.inSession.wrappedValue ? self.stopSession() : self.startSession()
        }.padding().onAppear {
            GlanceVisitor.add(glanceVisitorDelegate)
        }
    }
    
    func startSession(){
        GlanceVisitor.startSession()
    }
    
    func stopSession(){
        GlanceVisitor.endSession()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
