//
//  ScreenshareDemo_swiftuiApp.swift
//  ScreenshareDemo_swiftui
//
//  Created by Kyle Shank on 2/3/22.
//

import SwiftUI
import Glance_iOS

let GLANCE_GROUP_ID : Int32 = 15687

@main
struct ScreenshareDemo_swiftuiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().onAppear {
                self.glanceSetup()
            }
        }
    }
    
    func glanceSetup(){
//        GlanceSettings().set(kGlanceSettingGlanceServer, value: "www.myglance.net")
        GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
    }
}
