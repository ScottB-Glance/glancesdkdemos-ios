# Visitor SDK for iOS: Masking & WebView Masking

## Setup

Before proceeding, follow the instructions in the ScreenshareDemo_objc project's README to get the SDK setup in your app

### Masking

Views with personal private information or banking information should be hidden from the agent.  You can mask a view like this:

```swift
GlanceVisitor.addMaskedView(self.creditCardNumberTextField)
```

You can remove a mask with another call:

```swift
GlanceVisitor.removeMaskedView(self.creditCardNumberTextField)
```

You must remove any masked views before the view itself is deallocated.

### Masking within a WKWebView

Masking within a WKWebView is also supported. In order to take advantage of this, you'll need to include `GlanceMaskContentController` class which needs to be set as the `userContentController` via the `configuration` parameter passed during `WKWebView` intialization. The `GlanceMaskContentController` class should be initialized with comma separated strings of querySelectors and labels (optional). You may instead pass an array of strings as well.

For the list of query selectors, you can pass in anything that will be found via the `document.querySelectorAll()` javascript method. When the webview is loaded, we'll search for the given query selectors, find it's location on the screen, apply a mask over the entire element, and apply a label on top of the mask, if provided.

The following selectors will be masked by default:
- Any element with the class **glance_masked**
- Any element with the attribute **glance_masked="true"**
- Any text input element with **type="password"**

Additionally, a call to `setWebView` on `GlanceMaskContentController` should be made after the `WKWebView` instance has been created.  This is required in order to correctly mask elements with respect to where the `WKWebView` is located on the screen.

```objc
let querySelectors = "SELECTOR1,SELECTOR2,SELECTOR3,..."
let labels = "LABEL1,LABEL2,LABEL3,..."
let contentController : GlanceMaskContentController = GlanceMaskContentController(querySelectors, labels: labels)

let config = WKWebViewConfiguration()
config.userContentController = contentController;

self.webView = WKWebView(frame: self.view.frame, configuration: config)

contentController.setWebView(self.webView!)

let url = URL(string: maskingUrl)
self.webView?.load(URLRequest(url: url!))
```

On the agent side, the webview will be paused until it is fully loaded and the masks have been added. The webview will also pause when the user is scrolling within the webview, allowing the masks to be repositioned without the agent seeing any of the contents underneath.

This implementation also supports webpage mutations, along with masking within iframes.

#### Custom WKUserContentController

If your `WKWebView` implementation already has a custom `WKUserContentController` then have two options: subclass `GlanceMaskContentController` or manage your own instance of `GlanceMaskContentController`.

If you choose to subclass ensure a call to `setWebView` is still made after the `WKWebview` instance is created.

If you choose to create your own instance of `GlanceMaskContentController` ensure that the constructor is initialized properly with your custom `WKUserContentController` using `initWithUserContentController` initializers.  Additionally, ensure a call to `setWebView` is still made after the `WKWebview` instance is created.

## Support

Email [mobilesupport@glance.net](mailto:mobilesupport@glance.net) with any questions.
