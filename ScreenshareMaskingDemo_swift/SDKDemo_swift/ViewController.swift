//
//  ViewController.swift
//  SDKDemo_swift
//
//  Created by Ankit Desai on 4/6/18.
//  Copyright © 2018 Glance Networks. All rights reserved.
//

import UIKit

import Glance_iOS
import SafariServices

// Visitor demo implementation is below. NOTE:
// before reading through this code, make sure you've read through the README

let GLANCE_GROUP_ID : Int32 = 15687  // Get this from Glance

// make sure your interface implements GlanceVisitorDelegate
// Uncomment GlanceCustomViewerDelegate to test the custom agent viewer
class ViewController: UIViewController & GlanceVisitorDelegate, GlanceCustomViewerDelegate, GlanceDefaultUIDelegate {
    @IBOutlet weak var openBrowserButton: UIButton!
    @IBOutlet weak var startSessionButton: UIButton!
    @IBOutlet weak var endSessionButton: UIButton!
    @IBOutlet weak var sessionKeyLabel: UILabel!
    @IBOutlet weak var presenceIndicator: UILabel!
    
    var customWindow : UIWindow?
    
    var webBrowserNav : UINavigationController?
    var maskedWebView : WKWebView?
    var customuserContentController : WKUserContentController?
    var glanceMaskContentController : GlanceMaskContentController?
    
    // Uncomment the VISITOR_ID value to enable Presence code
    // If you are not using Presence you do not need to implement any code within if (VISITOR_ID != nil)
    var VISITOR_ID : String? // = "123four" // This is your identifier for the logged in (or identified) user.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Make sure the GlanceVisitor is listening for events on this ViewController
        GlanceVisitor.add(self)
        
        // Uncomment to test the custom agent viewer
        GlanceVisitor.setCustomViewerDelegate(self)
        
//        GlanceSettings().set(kGlanceSettingGlanceServer, value: "www.myglance.net")
        
        if (VISITOR_ID != nil) {
            // Init with visitor id for Presence, you would probably do this on user login
            GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "", visitorid: VISITOR_ID)
        } else {
            // Init for screensharing, you could do this on application launch
            GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
        }
        GlanceVisitor.defaultUI(true, delegate: self, termsUrl: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (VISITOR_ID != nil) {
            GlancePresenceVisitor.presence(["url": "main view"])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func openBrowser(_ sender: UIButton) {
        /**
         * CUSTOM WKUSERCONTENTCONTROLLER
         */
        self.customuserContentController = WKUserContentController()
        self.glanceMaskContentController = GlanceMaskContentController(userContentController: self.customuserContentController!, querySelectors:".mask_1, .mask_2, #mask_3, .mask_4, span, #hplogo", labels: "mask 1, mask 2, mask 3, mask 4, span, LOGO")

        let config = WKWebViewConfiguration()
        config.userContentController = self.customuserContentController!
        
        /**
         * DEFAULT - NO CUSTOM WKUSERCONTENTCONTROLLER
         * */
        
//        self.glanceMaskContentController = GlanceMaskContentController(".mask_1, .mask_2, #mask_3, .mask_4, span, #hplogo", labels: "mask 1, mask 2, mask 3, mask 4, span, LOGO")
//
//        let config = WKWebViewConfiguration()
//        config.userContentController = self.glanceMaskContentController!
        
        self.maskedWebView = WKWebView(frame: self.view.frame, configuration: config)
        self.glanceMaskContentController?.setWebView(self.maskedWebView!)
        
        let vc = UIViewController()
        vc.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(closeBrowser))
        vc.title = "Masked WebView"
        vc.view = self.maskedWebView
        
        self.webBrowserNav = UINavigationController(rootViewController: vc)
        
        let url = URL(string: "https://d2e93a2oavc15x.cloudfront.net")
        self.maskedWebView?.load(URLRequest(url: url!))
        
        present(self.webBrowserNav!, animated: true, completion: nil)
        
        if (VISITOR_ID != nil) {
            GlancePresenceVisitor.presence(["url": "webview https://www.glance.net"])
        }
    }
    
    @IBAction func closeBrowser(){
        self.webBrowserNav?.dismiss(animated: true, completion: nil)
        self.webBrowserNav = nil
    }
    
    @IBAction func startSession(_ sender: UIButton) {
        // Start session and let Glance generate a session key
        GlanceVisitor.startSession()
    }
    
    @IBAction func endSession(_ sender: UIButton) {
        // End Glance session
        GlanceVisitor.endSession()
    }
    
    func _sessionStarted(_ sessionKey: String) {
        DispatchQueue.main.async {
            self.sessionKeyLabel.text = sessionKey
            // Adds a masked view to sessionLabel which will keep it from being seen by the agent
            GlanceVisitor.addMaskedView(self.sessionKeyLabel)
            self.startSessionButton.isHidden = true
            self.endSessionButton.isHidden = false
        }
    }
    
    func _sessionEnded() {
        DispatchQueue.main.async {
            self.sessionKeyLabel.text = ""
            // Remove masked view
            GlanceVisitor.removeMaskedView(self.sessionKeyLabel)
            self.startSessionButton.isHidden = false
            self.endSessionButton.isHidden = true
        }
    }
    
    func alertWithTitle(_ title: String, _ message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Glance Visitor delegate
    func glanceVisitorEvent(_ event: GlanceEvent!) {
        switch event.code {
        case EventSessionEnded:
            // WAIT for this event to fire before starting a new session
            // Show any UI to indicate the session has ended
            _sessionEnded()
            break
        case EventConnectedToSession:
            // Show any UI to indicate the session has started
            // event.properties includes the sessionKey which can be
            // displayed to the user to read to the agent
            _sessionStarted(event.properties["sessionkey"] as! String)
            break
        case EventStartSessionFailed:
            _sessionEnded()
            break
        default:
            // Best practice is to log all other events of type EventError, EventWarning, or EventAssertFail
            switch(event.type) {
            case EventError:
                alertWithTitle("Error", event.message)
                break
            case EventAssertFail:
                alertWithTitle("Assert Fail", event.message)
                break
            case EventWarning:
                break
            default:
                break
            }
        }
        if (VISITOR_ID != nil) {
            switch event.code {
            case EventVisitorInitialized:
                DispatchQueue.main.async {
                    GlancePresenceVisitor.connect()
                }
                break;
            
            case EventPresenceConnected:
                DispatchQueue.main.async {
                    self.presenceIndicator.isHidden = false
                    self.startSessionButton.isHidden = true
                }
                break;
            
            case EventPresenceConnectFail:
                print("Presence connection failed (will retry): ", event.message)
                break;
            
            case EventPresenceShowTerms:
                // You only need to handle this event if you are providing a custom UI for terms and/or confirmation
                print("Agent signalled ShowTerms")
                break;
            
            case EventPresenceBlur:
                // This event notifies the app that the visitor is now using another app or website
                break;
            default:
                break;
            }
        }
    }
    
    // UNCOMMENT TO DEMO CUSTOM AGENT VIEWER
    
    /**
     Called when the agent viewer has stopped
     
     - Parameter glanceView: UIView displaying agent video. Add this view to your interface
     - Parameter size: Preferred size of the UIView
    
    */
    func glanceViewerDidStart(_ glanceView: UIView!, size: CGSize) {
        glanceView.layer.cornerRadius = 8.0
        glanceView.clipsToBounds = true

        DispatchQueue.main.async {
            self.customWindow?.resignKey()
            self.customWindow = nil

            self.customWindow = CustomViewerWindow(UIScreen.main.bounds, view: glanceView, size: size)
        }
    }
    
    /**
     Called when the agent viewer has stopped
     
     - Parameter glanceView: UIView displaying agent video. Remove this view to your interface
     
     */
    func glanceViewerDidStop(_ glanceView: UIView!) {
        if (self.customWindow != nil) {
            DispatchQueue.main.async {
                self.customWindow?.resignKey()
                self.customWindow?.isHidden = true
                self.customWindow = nil
            }
        }
    }
    
    func glanceDefaultUIDialogAccepted() {
        print("glanceDefaultUIDialogAccepted")
    }
    
    func glanceDefaultUIDialogCancelled() {
        print("glanceDefaultUIDialogCancelled")
    }
}

