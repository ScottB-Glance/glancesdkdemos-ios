//
//  ViewController.m
//  VideoDemo_objc
//
//  Created by Ankit Desai on 12/13/19.
//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import "ViewController.h"

#import <Glance_iOSVideo/Glance_iOS.h>

// Glance Video Demo implementation is below. NOTE:
// Before reading through this code, make sure you've read through the README

#define GLANCE_GROUP_ID 15687 // Get this from Glance
#define GLANCE_SESSION_KEY @"12345" // Key for this session, you should generate a random key

// Make sure your interface implements GlanceVideoDefaultUIDelegate
@interface ViewController () <GlanceVideoDefaultUIDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GlanceVisitor init:GLANCE_GROUP_ID token:@"" name:@"" email:@"" phone:@""];
    
//    Examples of customizing 2-Way Video Dialog colors:
//    [GlanceVisitor setVideoDefaultUIColor:DialogConfirmButtonBackgroundColor color:[UIColor greenColor]];
//    [GlanceVisitor setVideoDefaultUIColor:DialogConfirmButtonTitleColor color:[UIColor grayColor]];
//
//    [GlanceVisitor setVideoDefaultUIColor:DialogLinkColor color:[UIColor greenColor]];
//    [GlanceVisitor setVideoDefaultUIColor:DialogTextColor color:[UIColor yellowColor]];
//
//    [GlanceVisitor setVideoDefaultUIColor:DialogCancelButtonBackgroundColor color:[UIColor redColor]];
//    [GlanceVisitor setVideoDefaultUIColor:DialogCancelButtonTitleColor color:[UIColor blackColor]];
}

-(IBAction)startVideoSessionFrontCamera{
    NSLog(@"Starting Glance 2-Way Video via Front Camera");
    
    // Default implementation of Glance 2-Way Video
    [GlanceVisitor startVideoCall:GLANCE_SESSION_KEY delegate:self camera:@"front" termsUrl:@"https://ww2.glance.net/"];
//    To use without the dialog provided by Glance:
//    [GlanceVisitor startVideoCall:GLANCE_SESSION_KEY delegate:self camera:@"front" termsUrl:nil showDialog:NO];
}

-(IBAction)startVideoSessionBackCamera{
    NSLog(@"Starting Glance 2-Way Video via Back Camera");
    
    // Customize the video options sent to Glance
    [GlanceVisitor startVideoCall:GLANCE_SESSION_KEY delegate:self camera:@"back" preferredSize:CGSizeMake(0, 0) framerate:0 timeout:0 termsUrl:@"https://ww2.glance.net/"];
}

// Make sure to implement glanceVideoDefaultUIDidError as this will catch any errors that come up from the Glance Video SDK
-(void)glanceVideoDefaultUIDidError:(NSError*)error{
    NSLog(@"ERROR: %@", error.localizedDescription);
}

-(void)glanceVideoDefaultUIDialogAccepted{
    NSLog(@"glanceVideoDefaultUIDialogAccepted");
}

-(void)glanceVideoDefaultUIDialogCancelled{
    NSLog(@"glanceVideoDefaultUIDialogCancelled");
}

@end
