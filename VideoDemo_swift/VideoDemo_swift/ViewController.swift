//
//  ViewController.swift
//  VideoDemo_swift
//
//  Created by Ankit Desai on 1/28/20.
//  Copyright © 2020 Glance Networks, Inc. All rights reserved.
//

import UIKit
import Glance_iOSVideo

// Glance Video Demo implementation is below. NOTE:
// Before reading through this code, make sure you've read through the README

let GLANCE_GROUP_ID : Int32 = 15687 // Get this from Glance
let GLANCE_SESSION_KEY : String = "12345" // This is your identifier for the logged in (or identified) user.

class ViewController: UIViewController & GlanceVideoDefaultUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
        
//        Examples of customizing 2-Way Video Dialog colors:
//        GlanceVisitor.setVideoDefaultUIColor(GlanceDefaultUIColors.DialogConfirmButtonBackgroundColor, color: UIColor.green)
//        GlanceVisitor.setVideoDefaultUIColor(GlanceDefaultUIColors.DialogConfirmButtonTitleColor, color: UIColor.gray)
//
//        GlanceVisitor.setVideoDefaultUIColor(GlanceDefaultUIColors.DialogLinkColor, color: UIColor.green)
//        GlanceVisitor.setVideoDefaultUIColor(GlanceDefaultUIColors.DialogTextColor, color: UIColor.yellow)
//
//        GlanceVisitor.setVideoDefaultUIColor(GlanceDefaultUIColors.DialogCancelButtonBackgroundColor, color: UIColor.red)
//        GlanceVisitor.setVideoDefaultUIColor(GlanceDefaultUIColors.DialogCancelButtonTitleColor, color: UIColor.black)
    }

    @IBAction func startVideoSessionFrontCamera() {
        NSLog("Starting Glance 2-Way Video via Front Camera");
        
        // Default implementation of Glance 2-Way Video
        GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "front", termsUrl: "https://ww2.glance.net/")
//        To use without the dialog provided by Glance:
//        GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "front", termsUrl: nil, showDialog: false)
    }
    
    @IBAction func startVideoSessionBackCamera() {
        NSLog("Starting Glance 2-Way Video via Back Camera");
        
        // Customize the video options sent to Glance
        GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "back", preferredSize: CGSize.init(width: 0, height: 0), framerate: 0, timeout: 0, termsUrl: "https://ww2.glance.net/")
    }
    
    func glanceVideoDefaultUIDidError(_ error : Error!) {
        NSLog("ERROR: %@", error.localizedDescription);
    }
    
    func glanceVideoDefaultUIDialogAccepted() {
        NSLog("glanceVideoDefaultUIDialogAccepted")
    }
    
    func glanceVideoDefaultUIDialogCancelled() {
        NSLog("glanceVideoDefaultUIDialogCancelled")
    }

}

