# Glance 2-Way Video Demo Project

## Setup

TODO: Salesforce related setup information here

Obtain Group ID from Glance

## Quick start

Clone this project and replace the GLANCE_GROUP_ID value with your Group ID obtained from Glance.

Build for debugging. A Release build will require you to set the project Code Signing

## Installation

### Disclaimer

Glance SDK currently does NOT support SceneDelegate (introduced by Apple when creating new apps in Xcode 11+)

If you have an app with SceneDelegate, here are the steps to remove it:

1. Delete SceneDelgate.swift
2. Remove "Application Scene Manifest" key from Info.plist
3. Replace AppDelegate.swift with AppDelegate.swift found in this demo

Then, set the root view controller with the following code in the `application` function in AppDelegate (before `return true`):

```swift
let window = UIWindow(frame: UIScreen.main.bounds)
window.rootViewController = UIHostingController(rootView: MyInitialView()) // Replace with your initial view.
window.makeKeyAndVisible()
self.window = window
```

Make sure to `import SwiftUI` at the top of the file.

### Framework

Download the Glance iOS Video SDK or copy from this project
Add or copy the Glance_iOSVideo.framework to your Xcode project.
In your target settings, under "General", ensure that Glance_iOSVideo.framework is listed within Frameworks, Libraries, and Embedded Content with the Embed option of "Embed & Sign"

### Configuration

Now that you have the Glance Video framework imported into your Xcode project and have obtained your Glance Group ID you can get start integrating the 2-Way Video SDK.

SwiftUI's view based system makes it difficult for us to include Glance's API integration within the view files. Thus, we need to create a separate ObservableObject class that will handle our logic, which we can call from our views. The 2-Way Video implementation can be found in the `Glance.swift` file in this demo.

To call the ObservableObject class from your views, you'll need to slightly modify the code we added to set the root view controller above:

```swift
window.rootViewController = UIHostingController(rootView: ContentView().environmentObject(Glance.shared)) // Replace with your initial view.
```

See `Glance.swift` in this demo for more details about the `shared` object.

To import the SDK Framework headers:

```swift
import Glance_iOSVideo
```

Either in your AppDelegate or in your Glance.swift file, add the following configuration with your GLANCE_GROUP_ID number which is required.  The token, name, email and phone fields are optional.

```swift
// Configure Glance Visitor SDK
GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
```

Finally, make sure to allow your app to access the device's camera by adding the following key to your Info.plist file:

`NSCameraUsageDescription`

The value will be a string that the user will see before accepting camera permissions.

### 2-Way Video

In order to initiate a 2-Way Video Call, use the following:

```swift
GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "front", termsUrl: "https://ww2.glance.net/")
```

This will show the default dialog provided by Glance. It can be fully customized (see "Customizing Colors & Images" below)

The first parameter is a session key you provide that the agent will use to connect after the user has initiated a 2-Way Video Call.

The camera parameter takes in either "front" or "back". The user's device will start 2-Way Video with the camera based on the value passed in here.

The termsUrl takes in a string to a Terms & Conditions URL of your choosing.

To use your own custom dialog, you can disable the one provided by Glance with the `showDialog` argument:

```swift
GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "front", termsUrl: "https://ww2.glance.net/", showDialog: false)
```

For more customization options regarding video, Glance provides the following method:

```swift
GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "front", preferredSize: CGSize.init(width: 0, height: 0), framerate: 0, timeout: 0, termsUrl: "https://ww2.glance.net/")
```

Which can also be used without the dialog provided by Glance:

```swift
GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "front", preferredSize: CGSize.init(width: 0, height: 0), framerate: 0, timeout: 0, termsUrl: "https://ww2.glance.net/", showDialog: false)
```

### Error Handling

In order to catch errors produced by the SDK during a 2-Way Video Session, your AppDelegate or ViewController must implement the following delegate method:

```swift
func glanceVideoDefaultUIDidError(_ error : Error!) {
    NSLog("ERROR: %@", error.localizedDescription);
}
```

## Advanced

### Enable Glance Voice

To allow the user to speak to the agent via Glance's VoIP system:

1. Download the Twilio SDK for iOS version 2.0.7.
2. Extract the downloaded file, and find the file named TwilioVoice.framework.
3. Copy TwilioVoice.framework into your Xcode iOS project.
4. Add TwilioVoice.framework to Embedded Binaries within the Xcode project configuration.
5. Verify TwilioVoice.framework is also present within Linked Frameworks and Binaries.
6. Add the permission to use the microphone to the iOS application’s Info.plist file:
``<key>NSMicrophoneUsageDescription</key>
<string>Glance would like to use your microphone for customer support</string>
``

Then, instead of the startVideoCall method used in 2-Way Video section above, use the following:

```swift
GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: "front", preferredSize: CGSize.init(width: 0, height: 0), framerate: 0, timeout: 0, termsUrl: "https://ww2.glance.net/" voiceGroupId: GLANCE_VOICE_GROUP_ID, voiceApiKey: GLANCE_VOICE_API_KEY, voiceParameters: nil, muted: false)
```

Glance Voice requires a voice group ID and voice api key provided by Glance.

### Customizing Colors & Images

1. To set various colors used by the default UI:

```swift
GlanceVisitor.setVideoDefaultUIColor(GlanceDefaultUIColors.CONSTANT, color: UIColor.COLOR_VALUE)
```

Color constants can be found in the images below.

2. To override the 2-Way Video Default UI text and image resources, include resources with the same name within your own application bundle resources. An example of overriding text can be found in the Localizable.strings file.

![2-Way Video Customizations](../VideoDemo_objc/video_dialog_help.png "2-Way Video Customizations")

## Support

TODO: different support for 2-way video?

Email [mobilesupport@glance.net](mailto:mobilesupport@glance.net) with any questions.
