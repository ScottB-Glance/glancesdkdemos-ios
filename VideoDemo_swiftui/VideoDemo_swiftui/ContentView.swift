//
//  ContentView.swift
//  VideoDemo_swiftui
//
//  Created by Ankit Desai on 7/28/20.
//  Copyright © 2020 Glance Networks, Inc. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Spacer()
            Button(action: Glance.shared.startVideoCallFront) {
                Text("Start Glance 2-Way Video (Front Camera)")
            }
            Spacer()
            Button(action: Glance.shared.startVideoCallBack) {
                Text("Start Glance 2-Way Video (Back Camera)")
            }
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
