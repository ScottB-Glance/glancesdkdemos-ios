//
//  Glance.swift
//  VideoDemo_swiftui
//
//  Created by Ankit Desai on 7/28/20.
//  Copyright © 2020 Glance Networks, Inc. All rights reserved.
//

import Foundation
import Glance_iOSVideo

let GLANCE_GROUP_ID : Int32 = 15687 // Get this from Glance
let GLANCE_SESSION_KEY : String = "12345" // This is your identifier for the logged in (or identified) user.

class Glance: NSObject, ObservableObject, GlanceVideoDefaultUIDelegate {
    static let shared = Glance()
    
    func initVisitor() {
        GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
    }
    
    func startVideoCallFront() {
        startVideoCall(camera: "front")
    }
    
    func startVideoCallBack() {
        startVideoCall(camera: "back")
    }
    
    func startVideoCall(camera : String) {
        GlanceVisitor.startVideoCall(GLANCE_SESSION_KEY, delegate: self, camera: camera, termsUrl: "https://ww2.glance.net/")
    }
    
    func glanceVideoDefaultUIDidError(_ error: Error!) {
        print("ERROR: " + error.localizedDescription)
    }
    
    func glanceVideoDefaultUIDialogAccepted() {
        print("glanceVideoDefaultUIDialogAccepted")
    }
    
    func glanceVideoDefaultUIDialogCancelled() {
        print("glanceVideoDefaultUIDialogCancelled")
    }
}
