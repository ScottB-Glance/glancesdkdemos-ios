# Prerequisites
Before you use Glance Voice, verify that you have added the Glance Mobile SDK for iOS to your application. The Glance Mobile SDK includes Glance Voice functionality. You must also add the Twilio SDK to your project, as detailed below.

## Enable Glance Voice
If you had previously installed the legacy Glance VoIP SDK using Cocoapods, remove the dependency on GlanceVoice via Cocoapods, and manually add the Twilio SDK dependency to your iOS project per the documentation below.

To enable Glance Voice:

1. Download the [Twilio SDK for iOS version 6.3.0](https://github.com/twilio/twilio-voice-ios/releases/tag/6.3.0).
2. Extract the downloaded file, and find the file named TwilioVoice.xcframework.
3. Copy TwilioVoice.xcframework into your Xcode iOS project.
4. Add TwilioVoice.xcframework to Embedded Binaries within the Xcode project configuration.
5. Verify TwilioVoice.xcframework is also present within Linked Frameworks and Binaries.
6. Add the permission to use the microphone to the iOS application’s Info.plist file:
``<key>NSMicrophoneUsageDescription</key>
<string>Glance would like to use your microphone for customer support</string>
``

# Integration
There are two ways you can integrate Glance Voice into an app: the Default UI or a custom implementation.

- The Glance Default UI provides a Glance Voice implementation that will work out-of-the-box, in conjunction with Glance Screenshare for iOS.
- The custom implementation provides the ability to completely control the voice and interface experience.

## Default UI Configuration
Glance Voice requires a Twilio JWT authentication token that can be retrieved securely from your server. An example of how to do this can be found in this project. You must call the following to configure the default UI before a session is started with GlanceVisitor.startSession.

```
[GlanceVisitor defaultUI:YES delegate:self termsUrl:TERMS_URL muted:YES];
```

Then, add `GlanceDefaultUIDelegate` to your class, and implement the following two required methods:

```
-(void)glanceDefaultUIVoiceAuthenticationRequired;
-(void)glanceDefaultUIVoiceAuthenticationFailed;
```

`glanceDefaultUIVoiceAuthenticationRequired` will get called after the user starts a session via the DefaultUI dialog. In this method, you can make a call to your server to retrieve the authentication token. Once you've done that, make a call to `Visitor.defaultUIVoiceStartCall` with the auth token to connect to Glance Voice:

`[GlanceVisitor defaultUIVoiceStartCall:YOUR_AUTH_TOKEN];`

That's it! You're all set.

## Custom Configuration

If you are not going to use the default UI, you must configure your custom implementation to use Glance Voice.

Import the Glance Voice header into your implementation:

```
#import <Glance_iOS/GlanceVoice.h>
```

### Availability
Glance Voice requires the Twilio SDK to work. Use the following methods to quickly check if Glance Voice is ready to be used and available.

```
if ([GlanceVoice isAvailable]){
// Twilio SDK present and GlanceVoice is available
}
```

### Events
If you’re doing a custom implementation, you must implement a delegate or listener to react events. Here are the protocols or interfaces for the delegates and listeners and how to set them up.

```
@protocol GlanceVoiceDelegate <NSObject>

@optional

- (void) glanceVoiceAuthenticateDidSucceed:(NSString *)value;
- (void) glanceVoiceAuthenticateDidFail:(NSError*) error;

- (void) glanceVoiceStartDidSucceed;
- (void) glanceVoiceStartDidFail:(NSError*)error;
- (void) glanceVoiceDidDisconnectWithError:(NSError*)error;
- (void) glanceVoiceReconnecting;
- (void) glanceVoiceDidReconnect;
- (void) glanceVoiceReconnectDidTimeout;

@end
```

```
[GlanceVoice setDelegate: this];
```

### Authenticate
With the delegate or listener in place,  now you can make the first call to Glance Voice to authenticate with the Glance Voice group ID and API  key.

```
[GlanceVoice authenticate: 1234 apiKey: @”yourApiKey”];
```

### Starting a Voice Session
The call to authenticate results in your delegate or listener invoking when the call succeeds or fails. If authentication succeeds, the JSON string passed to the delegate or listener method is required when starting a session. Verify that startCall does in fact succeed or manage its failure with the methods within the delegate or listener.

```
- (void)glanceVoiceAuthenticateDidSucceed:(NSString *)authenticationValue {
[GlanceVoice startCall:authenticationValue];
}
```

### Ending a VoiceSession
To end a voice session, invoke endCall.

```
[GlanceVoice endCall];
```

### Mute or Unmute a Session
Set the mute state with a boolean yes/no or true/false.

```
[GlanceVoice setMuted: YES];
```

### Enable or Disable Speakerphone
Toggle whether or not the audio of the call is played through the speaker of the phone.

```
[GlanceVoice setSpeakerphoneEnabled:YES];
```
