//
//  AppDelegate.h
//  VoiceDemo_objc
//
//  Created by Ankit Desai on 7/22/19.
//  Copyright © 2019 Glance Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

