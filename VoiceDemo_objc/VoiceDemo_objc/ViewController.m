//
//  ViewController.m
//  VoiceDemo_objc
//
//  Created by Ankit Desai on 7/22/19.
//  Copyright © 2019 Glance Networks. All rights reserved.
//

#import "ViewController.h"

#import <Glance_iOS/Glance_iOS.h>
#import <Glance_iOS/GlanceVoice.h>

/*
 Demo implementation of GlanceVoice. Before proceeding, make sure you've read the README.
 
 - There are two ways of implementing GlanceVoice: With or without Glance's Default UI
 - Both ways are demonstrated as a part of this demo. DefaultUI is on initially
 - Set the GLANCE_USE_DEFAULT_UI flag to false to use the custom implementation
 - Depending on which way you implement GlanceVoice, you'll need a specific Listener:
 = If Default UI is enabled, GlanceDefaultUIDelegate needs to be implemented
 = If you're using a custom implementation, GlanceVoiceDelegate needs to be implemented
 = NOTE: Both delegates do NOT need to be implemented at the same time. This is only the case here to demonstrate both use cases
 */

// Get these from Glance:
#define GLANCE_GROUP_ID 15687
#define GLANCE_SESSION_KEY @"12345"
#define GLANCE_USE_DEFAULT_UI TRUE
#define GLANCE_VOICE_GROUP_ID 1268
#define GLANCE_VOICE_API_KEY @"twiliotest2017"
#define GLANCE_TERMS_URL @"https://ww2.glance.net/terms/"

@interface ViewController () <GlanceVisitorDelegate, GlanceDefaultUIDelegate, GlanceVoiceDelegate>
@property (weak, nonatomic) IBOutlet UIButton* startSessionButton;
@property (weak, nonatomic) IBOutlet UILabel* sessionKeyLabel;
@property (weak, nonatomic) IBOutlet UILabel* voiceNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel* voicePinLabel;

@property (weak, nonatomic) IBOutlet UIButton* endSessionButton;
@property (weak, nonatomic) IBOutlet UIButton* muteButton;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Make sure the GlanceVisitor is listening for events on this ViewController
    [GlanceVisitor addDelegate:self];
    
//    [[[GlanceSettings alloc] init] set:kGlanceSettingGlanceServer value:@"www.myglance.net"];
    
    [GlanceVisitor initVisitor:GLANCE_GROUP_ID token:@"" name:@"" email:@"" phone:@"" visitorid:GLANCE_SESSION_KEY];
    [GlanceVisitor defaultUI:YES delegate:self termsUrl:nil];
    
    #if !GLANCE_USE_DEFAULT_UI
        [GlanceVisitor defaultUI:false];
        [GlanceVoice setDelegate:self];
    #endif
}

-(IBAction)startSession:(id)sender {
    [GlanceVisitor defaultUI:YES delegate:self termsUrl:GLANCE_TERMS_URL muted:NO];
    [GlanceVisitor startSession];
    #if !GLANCE_USE_DEFAULT_UI
    // Get voiceGroupId & voiceApiKey from Glance
    [GlanceVoice authenticate:GLANCE_VOICE_GROUP_ID apiKey:GLANCE_VOICE_API_KEY];
    #endif
}

-(void)_sessionStarted:(NSString*)sessionKey {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.startSessionButton setHidden:YES];
        // Adds a masked view to sessionLabel which will keep it from being seen by the agent
        [GlanceVisitor addMaskedView:self.sessionKeyLabel];
        self.sessionKeyLabel.text = [NSString stringWithFormat:@"Session Key: %@", sessionKey];
        
        #if !GLANCE_USE_DEFAULT_UI
        [self.endSessionButton setHidden:NO];
        [self.muteButton setHidden:NO];
        #endif
    });
}

-(void)_sessionEnded {
    dispatch_async(dispatch_get_main_queue(), ^{
        // Remove masked view
        [GlanceVisitor removeMaskedView:self.sessionKeyLabel];
        
        [self.startSessionButton setHidden:NO];
        self.sessionKeyLabel.text = @"";
        self.voiceNumberLabel.text = @"";
        self.voicePinLabel.text = @"";
        
        #if !GLANCE_USE_DEFAULT_UI
        [self.endSessionButton setHidden:YES];
        [self.muteButton setHidden:YES];
        #endif
    });
}

// Alert
- (void)alertWithTitle:(NSString *)title message:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* a = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:a];
        
        [self presentViewController:alert animated:true completion:nil];
    });
}

// Glance Visitor delegate
-(void)glanceVisitorEvent:(GlanceEvent*)event{
    switch (event.code) {
        case EventConnectedToSession:
            // Show any UI to indicate the session has started
            // event.properties includes the sessionKey which can be
            // displayed to the user to read to the agent
            [self _sessionStarted: event.properties[@"sessionkey"]];
            break;
            
        case EventStartSessionFailed:
            [self alertWithTitle: @"Start Session Failed" message: event.message];
            [self _sessionEnded];
            break;
            
        case EventSessionEnded:
            // WAIT for this event to fire before starting a new session
            // Show any UI to indicate the session has ended
            [self _sessionEnded];
            break;
        default:
            // Best practice is to log all other events of type EventError, EventWarning, or EventAssertFail
            switch (event.type) {
                case EventError:
                    [self alertWithTitle: @"Error" message: event.message];
                    break;
                    
                case EventAssertFail:
                    [self alertWithTitle: @"Assert Fail" message: event.message];
                    break;
                    
                case EventWarning:
                default:
                    break;
            }
            break;
    }
}

// Start GlanceDefaultUIDelegate methods
-(void)glanceDefaultUIVoiceAuthenticationRequired{
    NSLog(@"glanceDefaultUIVoiceAuthenticationRequired");
    
    // Get token string
    NSString * kYourServerBaseURLString = @"https://voice.glance.net";
    NSString * kAccessTokenEndpoint = @"/services/voice/SetupCall";
    
    NSString* queryString = [NSString stringWithFormat:@"?groupid=%@&apikey=%@", @(GLANCE_VOICE_GROUP_ID), GLANCE_VOICE_API_KEY];
    
    NSString *accessTokenURLString = [[kYourServerBaseURLString stringByAppendingString:kAccessTokenEndpoint] stringByAppendingString:queryString];
    
    NSError* error;
    NSString *tokenJSON = [NSString stringWithContentsOfURL:[NSURL URLWithString:accessTokenURLString]
                                                   encoding:NSUTF8StringEncoding
                                                      error:&error];
    
    [GlanceVisitor defaultUIVoiceStartCall:tokenJSON];
    
    NSError* jsonError = nil;
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData: [tokenJSON dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &jsonError];
    if (dict[@"number"] != nil && dict[@"pin"] != nil) {
        self.voiceNumberLabel.text = [NSString stringWithFormat:@"Phone Number: %@", dict[@"number"]];
        self.voicePinLabel.text = [NSString stringWithFormat:@"PIN: %@", dict[@"pin"]];
    }
}

-(void)glanceDefaultUIVoiceAuthenticationFailed{
    NSLog(@"glanceDefaultUIVoiceAuthenticationFailed");
    [self alertWithTitle:@"Voice Error" message:@"Couldn't authenticate with Twilio"];
}
// End GlanceDefaultUIDelegate methods

// Start GlanceVoice - Custom Implementation - ONLY USED IF DEFAULT UI IS NOT ENABLED
-(IBAction)mute:(id)sender {
    bool muted = [GlanceVoice isMuted];
    NSString *mutedString = !muted ? @"Mute" : @"Unmute";
    [GlanceVoice setMuted:!muted];
    [self.muteButton setTitle:mutedString forState:UIControlStateNormal];
}

-(IBAction)endSession:(id)sender {
    #if !GLANCE_USE_DEFAULT_UI
    if ([GlanceVoice isAvailable]) {
        [GlanceVoice endCall];
    }
    #endif
    [GlanceVisitor endSession];
}
// End GlanceVoice - Custom Implementation

-(void)glanceDefaultUIDialogAccepted {
    NSLog(@"glanceDefaultUIDialogAccepted");
}

-(void)glanceDefaultUIDialogCancelled {
    NSLog(@"glanceDefaultUIDialogCancelled");
}

@end
