//
//  main.m
//  VoiceDemo_objc
//
//  Created by Ankit Desai on 7/22/19.
//  Copyright © 2019 Glance Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
