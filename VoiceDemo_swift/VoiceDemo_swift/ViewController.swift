//
//  ViewController.swift
//  VoiceDemo_swift
//
//  Created by Ankit Desai on 7/22/19.
//  Copyright © 2019 Glance Networks. All rights reserved.
//

import UIKit

import Glance_iOS

class ViewController: UIViewController & GlanceVisitorDelegate & GlanceDefaultUIDelegate & GlanceVoiceDelegate {
    @IBOutlet weak var startSessionButton: UIButton!
    @IBOutlet weak var sessionKeyLabel : UILabel!
    @IBOutlet weak var voiceNumberLabel : UILabel!
    @IBOutlet weak var voicePinLabel : UILabel!
    
    @IBOutlet weak var endSessionButton : UIButton!
    @IBOutlet weak var muteButton : UIButton!
    
    // Get these from Glance
    let GLANCE_GROUP_ID : Int32 = 15687
    let GLANCE_SESSION_KEY : String = "12345"
    let GLANCE_VOICE_GROUP_ID : Int32 = 1268
    let GLANCE_VOICE_API_KEY : String = "twiliotest2017"
    let GLANCE_USE_DEFAULT_UI : Bool = true
    let GLANCE_TERMS_URL : String = "https://ww2.glance.net/terms/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Make sure the GlanceVisitor is listening for events on this ViewController
        GlanceVisitor.add(self)
        
//        GlanceSettings()?.set(kGlanceSettingGlanceServer, value: "www.myglance.net")
        
        GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "", visitorid: GLANCE_SESSION_KEY)
        GlanceVisitor.defaultUI(true, delegate: self, termsUrl: nil)
        
        // Get voiceGroupId & voiceApiKey from Glance
        if (!GLANCE_USE_DEFAULT_UI) {
            GlanceVisitor.defaultUI(false)
            GlanceVoice.setDelegate(self)
        }
    }
    
    @IBAction func startSession(_ sender: UIButton) {
        GlanceVisitor.defaultUI(true, delegate: self, termsUrl: GLANCE_TERMS_URL, muted: false)
        GlanceVisitor.startSession()
        
        if (!GLANCE_USE_DEFAULT_UI) {
            GlanceVoice.authenticate(1268, apiKey: "twiliotest2017")
        }
    }
    
    func _sessionStarted(_ sessionKey : String) {
        DispatchQueue.main.async {
            self.sessionKeyLabel.text = "Session Key: " + sessionKey
            
            GlanceVisitor.addMaskedView(self.sessionKeyLabel)
            self.startSessionButton.isHidden = true
            
            if (!self.GLANCE_USE_DEFAULT_UI) {
                self.endSessionButton.isHidden = false
                self.muteButton.isHidden = false
            }
        }
    }
    
    func _sessionEnded() {
        DispatchQueue.main.async {
            self.sessionKeyLabel.text = ""
            self.voiceNumberLabel.text = ""
            self.voicePinLabel.text = ""
            
            GlanceVisitor.removeMaskedView(self.sessionKeyLabel)
            self.startSessionButton.isHidden = false
            
            if (!self.GLANCE_USE_DEFAULT_UI) {
                self.endSessionButton.isHidden = true
                self.muteButton.isHidden = true
            }
        }
    }

    // Glance Visitor delegate
    func glanceVisitorEvent(_ event: GlanceEvent!) {
        switch event.code {
        case EventConnectedToSession:
            // Show any UI to indicate the session has started
            // event.properties includes the sessionKey which can be
            // displayed to the user to read to the agent
            _sessionStarted(event.properties["sessionkey"] as! String)
            break
        case EventSessionEnded:
            // WAIT for this event to fire before starting a new session
            // Show any UI to indicate the session has ended
            _sessionEnded()
            break
        case EventStartSessionFailed:
            _sessionEnded()
            break
        default:
            // Best practice is to log all other events of type EventError, EventWarning, or EventAssertFail
            break
        }
    }
    
    // Start GlanceDefaultUIDelegate methods
    func glanceDefaultUIVoiceAuthenticationRequired() {
        print("glanceDefaultUIVoiceAuthenticationRequired")
        
        // Get token string
        let kYourServerBaseURLString = "https://voice.glance.net"
        let kAccessTokenEndpoint = "/services/voice/SetupCall"
        
        let queryString = String(format: "?groupid=%d&apikey=%@", GLANCE_VOICE_GROUP_ID, GLANCE_VOICE_API_KEY)
        
        let accessTokenURLString = kYourServerBaseURLString + kAccessTokenEndpoint + queryString
        do {
            let tokenJSON = try String(contentsOf: URL(string: accessTokenURLString)!)
            GlanceVisitor.defaultUIVoiceStartCall(tokenJSON)
            
            do {
                let dict = try JSONSerialization.jsonObject(with: tokenJSON.data(using: .utf8)!, options: .mutableContainers) as! Dictionary<String,Any>
                
                self.voiceNumberLabel.text = "Phone Number: " + (dict["number"] as! String)
                self.voicePinLabel.text = "PIN: " + (dict["pin"] as! String)
            } catch {
                print("Error serializing tokenJSON: %@", error)
            }
        } catch {
            print("Error getting token JSON: %@", error)
        }
    }
    
    func glanceDefaultUIVoiceAuthenticationFailed() {
        print("glanceDefaultUIVoiceAuthenticationFailed");
    }
    
    func glanceDefaultUIDialogAccepted() {
        print("glanceDefaultUIDialogAccepted")
    }
    
    func glanceDefaultUIDialogCancelled() {
        print("glanceDefaultUIDialogCancelled")
    }
    // End GlanceDefaultUIDelegate methods
    
    // Start GlanceVoice - Custom Implementation - ONLY USED IF DEFAULT UI IS NOT ENABLED
    @IBAction func mute(_ sender: UIButton) {
        let muted : Bool = GlanceVoice.isMuted()
        let mutedString : String = !muted ? "Mute" : "Unmute";
        GlanceVoice.setMuted(!muted)
        self.muteButton.setTitle(mutedString, for: .normal)
    }
    
    @IBAction func endSession(_ sender: UIButton) {
        if (!GLANCE_USE_DEFAULT_UI && GlanceVoice.isAvailable()) {
            GlanceVoice.endCall()
        }
        GlanceVisitor.endSession()
    }
    
    func glanceVoiceAuthenticateDidSucceed(_ value: String!) {
        let dict = try? JSONSerialization.jsonObject(with: value.data(using: .utf8)!, options:.mutableContainers) as? [String:AnyObject]
        
        DispatchQueue.main.async {
            if dict!["number"] != nil && dict!["pin"] != nil {
                self.voiceNumberLabel.text = "Phone Number: " + (dict!["number"] as! String)
                self.voicePinLabel.text = "PIN: " + (dict!["pin"] as! String)
            }
        }
    }
    // End GlanceVoice - Custom Implementation
}

